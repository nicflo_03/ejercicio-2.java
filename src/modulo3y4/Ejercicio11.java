package modulo3y4;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		
		Scanner escaner = new Scanner(System.in);
		
		char letr;
		
		System.out.print("Ingrese una letra = ");
		letr = escaner.next().charAt(0);
		
		if(letr=='a'||letr=='e'||letr=='i'||letr=='o'||letr=='u') {
			System.out.println(letr+" es una vocal");
		}
		else {
			System.out.println(letr+" es una consonante");
		}
		escaner.close();
	}

}

