package modulo3y4;

import java.util.Scanner;

public class Ejercicio8 {

	public static void main(String[] args) {
		
		Scanner escaner = new Scanner(System.in);
		
		int messi, cristiano;
		
		System.out.println(" Piedra Papel Tijeras");
		System.out.println("   (1)   (2)    (3)  ");
		
		System.out.print("barcelona =  ");
		messi = escaner.nextInt();
		System.out.print("real madrid = ");
		cristiano = escaner.nextInt();
		
		if(messi==0) {
			if(cristiano==1) {
				System.out.println("real madrid, GANADOR DE LA COPA");
			}
			else if(cristiano==2) {
				System.out.println("barcelona, GANADOR DE LA COPA");
			}
			else {
				System.out.println("Empate aburrido");
			}
		}
		else if(messi==1) {
			if(cristiano==0) {
				System.out.println("barcelona, GANADOR DE LA COPA");
			}
			else if(cristiano==2) {
				System.out.println("real madrid, GANADOR DE LA COPA");
			}
			else {
				System.out.println("Empate aburrido");
			}
		}
		else if(messi==2) {
			if(cristiano==0) {
				System.out.println("real madrid, GANADOR DE LA COPA");
			}
			else if(cristiano==1) {
				System.out.println("barcelona, GANADOR DE LA COPA");
			}
			else {
				System.out.println("Empate aburrido");
			}
		}
		escaner.close();
	}

}

